import * as mongoose from 'mongoose';

const jwtTokenSchema = new mongoose.Schema({
  identifyId: {
    type: String,
    required: true,
    unique: true,
  },
  token: {
    type: String,
    required: true,
    unique: true,
  },
  expireDate: {
    type: Date,
    default: Date.now,
    expires: '3600s',
  },
});

export default mongoose.model('JwtToken', jwtTokenSchema);
