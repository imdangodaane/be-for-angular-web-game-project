import * as mongoose from 'mongoose';
const Schema = mongoose.Schema;

const gameScoreSchema = new mongoose.Schema({
  game: {
    type: String,
    required: true,
  },
  score: {
    type: Number,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
  updatedDate: {
    type: Date,
    default: Date.now,
  },
});

const scoreSchema = new mongoose.Schema({
  identifyId: {
    type: Schema.Types.ObjectId,
    required: true,
    unique: true,
  },
  games: {
    type: [gameScoreSchema],
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
  updatedDate: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.model('Score', scoreSchema);
