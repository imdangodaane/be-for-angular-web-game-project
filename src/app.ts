import * as express from 'express';
import * as cors from 'cors';
import * as compression from 'compression';
import * as dotenv from 'dotenv';
import * as mongoose from 'mongoose';
import * as bodyParser from 'body-parser';
import router from './router/router';
import { containerLoader } from './loaders';

dotenv.config();

export class App {
  app: express.Application;

  constructor() {
    this.app = express();
    this.app.use(express.json());
    this.app.use(cors());
    this.app.use(compression());
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
  }

  public async start() {
    const PORT = process.env.PORT;
    const container = containerLoader();

    const mongodbConnectionString = await this.mongodbLoader();
    console.log(`[${new Date().toLocaleString()}]\tMongoDB connected`);

    this.app.use('/', router(container));

    this.app.listen(PORT, (err: Error) => {
      if (err) {
        console.table(err);
        return;
      }
      console.log(`[${new Date().toLocaleString()}]\tServer started and listen on PORT: ${PORT}`);
    });
  }

  public async mongodbLoader() {
    const connection = await mongoose.connect(process.env.MONGODB_DATABASE_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
    return connection.connection.db;
  }
}
