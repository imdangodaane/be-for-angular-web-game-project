export interface IHashService {
  genPassword(plaintext: string): string;
  checkPassword(password: string, hash: string): Promise<boolean>;
}
