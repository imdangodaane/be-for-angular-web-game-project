export interface IScoreService {
  getAll(type: string);
  updateScore(payload: IUpdateScorePayload);
}

export interface IUpdateScorePayload {
  identifyId: string;
  score: number;
  game: string;
}
