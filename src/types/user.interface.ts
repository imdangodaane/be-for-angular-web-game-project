import { Document } from 'mongoose';

export interface IUserService {
  create(payload: IUserRegisterPayload): Promise<Document>;
  find(username: string): Promise<Pick<Document, '_id'>>;
  findAll();
  update();
  delete();
  login(payload: IUserLoginPayload);
}

export interface IUserRegisterPayload {
  username: string;
  password: string;
  email: string;
}

export interface IUserLoginPayload {
  username: string;
  password: string;
}
