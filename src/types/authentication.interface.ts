export interface IAuthenticationService {
  authenticate(token: string): Promise<any>;
}
