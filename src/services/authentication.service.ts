import * as jwt from 'jsonwebtoken';
import jwtTokenModel from '../models/jwt-token.model';
import { IAuthenticationService } from '../types/authentication.interface';
import { injectable } from 'inversify';

@injectable()
export class AuthenticationService implements IAuthenticationService {
  constructor() {}

  public async authenticate(token: string): Promise<any> {
    const foundToken = await jwtTokenModel.findOne({ token }).lean();
    if (!foundToken) {
      throw new Error('Fail to authenticate');
    }
    const decodedDto = jwt.decode(token);
    return decodedDto;
  }
}
