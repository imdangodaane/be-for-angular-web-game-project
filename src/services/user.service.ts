import { IUserService, IUserRegisterPayload, IUserLoginPayload } from '../types/user.interface';
import { injectable, inject } from 'inversify';
import { IHashService } from '../types/hash.interface';
import userModel from '../models/user.model';
import jwtTokenModel from '../models/jwt-token.model';
import { Document } from 'mongoose';
import * as jwt from 'jsonwebtoken';
import * as dotenv from 'dotenv';

dotenv.config();

@injectable()
export class UserService implements IUserService {
  constructor(
    @inject('IHashService') private hashService: IHashService,
  ) {}

  public async create(payload: IUserRegisterPayload): Promise<Document> {
    if (Object.values(payload).some((value: string) => !value)) {
      throw new Error('Username or password or email empty');
    }
    const foundUser = await userModel.findOne({ username: payload.username }).lean();
    if (foundUser) {
      throw new Error('Username existed');
    }
    const hashedPassword = this.hashService.genPassword(payload.password);
    const newUser = await userModel.create({
      username: payload.username,
      password: hashedPassword,
      email: payload.email,
    });
    await newUser.save();
    return newUser;
  }

  public async find(username: string): Promise<Pick<Document, '_id'>> {
    const foundUser = await userModel.findOne({ username }).lean();
    if (!foundUser) {
      throw new Error('User not found');
    }
    return foundUser;
  }

  findAll() {

  }

  update() {

  }

  delete() {

  }

  public async login(payload: IUserLoginPayload) {
    const user = await this.find(payload.username);
    const match = await this.hashService.checkPassword(payload.password, user['password']);

    if (!match) {
      throw new Error('Failed to verify');
    }

    await jwtTokenModel.findOneAndDelete({ identifyId: user._id }, () => {
      console.log(`[${new Date().toLocaleString()}] Deleted previous token of user: ${user._id}`);
    });

    const expiredTime = new Date();
    expiredTime.setSeconds(expiredTime.getSeconds() + 3600);

    const tokenString = jwt.sign(
      {
        id: user._id,
        username: user['username'],
        exp: expiredTime.getTime(),
      },
      process.env.JWT_SECRET,
    );

    const newToken = await jwtTokenModel.create({
      identifyId: user._id,
      token: tokenString,
    });

    return newToken['token'];
  }
}
