import * as bcrypt from 'bcrypt';
import { IHashService } from '../types/hash.interface';
import { injectable } from 'inversify';

@injectable()
export class HashService implements IHashService {
  private saltRounds = 10;

  constructor() {}

  public genPassword(plaintext: string): string {
    const hash = bcrypt.hashSync(plaintext, this.saltRounds);
    return hash;
  }

  public async checkPassword(password: string, hash: string): Promise<boolean> {
    const match = await bcrypt.compare(password, hash);
    return match;
  }
}
