import { injectable } from 'inversify';
import { IScoreService, IUpdateScorePayload } from '../types/score.interface';
import scoreModel from '../models/score.model';
import userModel from '../models/user.model';
import * as mongoose from 'mongoose';

@injectable()
export class ScoreService implements IScoreService {
  constructor() {}

  public async getAll(type: string) {
    const res = await scoreModel.aggregate(
      [
        {
          $match: {
            'games.game': 'flappybird',
          },
        },
      ],
    )
    .lookup({
      from: 'users',
      localField: 'identifyId',
      foreignField: '_id',
      as: 'user',
    })
    .project({
      _id: 0,
      user: {
        username: 1,
      },
      games: {
        score: 1,
        game: 1,
      },
    })
    .unwind('user', 'games');
    return res;
  }

  public async updateScore(payload: IUpdateScorePayload) {
    let userScoreEntity = await scoreModel.findOne({ identifyId: payload.identifyId });

    if (!userScoreEntity) {
      const newUserScoreEntity = await scoreModel.create({
        identifyId: payload.identifyId,
        games: [
          {
            game: payload.game,
            score: payload.score,
          },
        ],
      });
      await newUserScoreEntity.save();
      return newUserScoreEntity;
    }

    scoreModel.updateOne(
      {
        _id: userScoreEntity._id,
        'games.game': 'flappybird',
        'games.score': { $lt: payload.score },
      },
      {
        $set: {
          'games.$.score': payload.score,
        },
      },
      (err) => { console.error(err); },
    );

    userScoreEntity = await scoreModel.findOne({ identifyId: payload.identifyId });

    return userScoreEntity;
  }
}
