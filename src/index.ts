import 'reflect-metadata';
import { App } from './app';

const myApp = new App();

myApp.start();
