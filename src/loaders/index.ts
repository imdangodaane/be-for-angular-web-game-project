import { Container } from 'inversify';
import { IUserService } from '../types/user.interface';
import { UserService } from '../services/user.service';
import { IHashService } from '../types/hash.interface';
import { HashService } from '../services/hash.service';
import { IAuthenticationService } from '../types/authentication.interface';
import { AuthenticationService } from '../services/authentication.service';
import { IScoreService } from '../types/score.interface';
import { ScoreService } from '../services/score.service';

export const containerLoader = () => {
  const container = new Container();

  container.bind<IUserService>('IUserService').to(UserService);
  container.bind<IHashService>('IHashService').to(HashService);
  container.bind<IAuthenticationService>('IAuthenticationService').to(AuthenticationService);
  container.bind<IScoreService>('IScoreService').to(ScoreService);

  return container;
};
