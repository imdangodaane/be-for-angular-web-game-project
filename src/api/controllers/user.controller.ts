import * as express from 'express';
import { Container } from 'inversify';
import { IUserService, IUserRegisterPayload, IUserLoginPayload } from '../../types/user.interface';

export default (container: Container) => {
  const router = express.Router();
  const userService = container.get<IUserService>('IUserService');

  router.post('/register', async (req, res) => {
    const payload: IUserRegisterPayload = {
      username: req.body['username'],
      password: req.body['password'],
      email: req.body['email'],
    };
    try {
      const newUser = await userService.create(payload);
      res.json(newUser);
    } catch (err) {
      res.status(400).json({
        message: err['message'],
      });
    }
  });

  router.post('/login', async (req, res) => {
    const payload: IUserLoginPayload = {
      username: req.body['username'],
      password: req.body['password'],
    };
    try {
      const token = await userService.login(payload);
      res.json({ token });
    } catch (err) {
      res.status(400).json({
        message: err['message'],
      });
    }
  });

  return router;
};
