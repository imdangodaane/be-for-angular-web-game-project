import * as express from 'express';
import { Container } from 'inversify';
import { IAuthenticationService } from '../../types/authentication.interface';
import { IScoreService, IUpdateScorePayload } from '../../types/score.interface';

export default (container: Container) => {
  const router = express.Router();
  const authenticationService = container.get<IAuthenticationService>('IAuthenticationService');
  const scoreService = container.get<IScoreService>('IScoreService');

  router.get('/flappybird', async (req, res) => {
    try {
      const result = await scoreService.getAll('flappybird');
      res.json(result);
    } catch (err) {
      res.status(400).json({ message: err['message'] });
    }
  });

  router.post('/flappybird', async (req, res) => {
    try {
      const authenticatedInfo  = await authenticationService.authenticate(
        req.headers['authorization'].split('Bearer ')[1],
      );
      const payload: IUpdateScorePayload = {
        identifyId: authenticatedInfo.id,
        score: req.body['score'],
        game: 'flappybird',
      };
      const updatedScore = await scoreService.updateScore(payload);
      res.json(updatedScore);
    } catch (err) {
      res.status(400).json({ message: err['message'] });
    }
  });

  return router;
};
