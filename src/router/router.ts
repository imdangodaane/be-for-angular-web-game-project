import * as express from 'express';
import { Container } from 'inversify';
import userController from '../api/controllers/user.controller';
import scoreController from '../api/controllers/score.controller';

export default (container: Container) => {
  const router = express.Router();

  router.get('/', (req, res) => {
    res.json('Hello world');
  });

  router.use('/user', userController(container));
  router.use('/score', scoreController(container));

  return router;
};
